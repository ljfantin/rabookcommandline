//
//  RaProcessPictureOperation.h
//  RaBook
//
//  Created by Leandro Fantin on 28/11/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RaProcessPictureOperationCompletionBlock)(NSString *filePath);
typedef void (^RaProcessPictureOperationCancelBlock)(void);
typedef void (^RaProcessPictureOperationErrorBlock)(NSError *error);

@interface RaProcessPictureOperation : NSOperation

- (instancetype)initWithImage:(NSImage *)image completionBlock:(RaProcessPictureOperationCompletionBlock)successBlock cancelBlock:(RaProcessPictureOperationCancelBlock)cancelBlock errorBlock:(RaProcessPictureOperationErrorBlock)errorBlock;

@end
