//
//  RAPictureConfig.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 3/7/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAArgumentedRealityObjectModel.h"

@interface RAPictureConfig : NSObject

@property (nonatomic, strong, readonly) NSURL *picturePath;
@property (nonatomic, strong, readonly) NSURL *thumbnailUrl;
@property (nonatomic, copy, readonly) NSString *layerIdentifier;
@property (nonatomic, assign, readonly) RAArgumentedRealityObjectLayerType layerType;

- (instancetype)initWithDictionary:(NSDictionary *)config;

@end
