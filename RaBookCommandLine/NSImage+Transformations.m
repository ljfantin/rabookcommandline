//
//  NSImage+Transformations.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 9/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "NSImage+Transformations.h"

@implementation NSImage (Transformations)

- (NSImage *)ra_imageByScalingToSize:(CGSize)targetSize
{
//    UIGraphicsBeginImageContextWithOptions(targetSize, NO, 0);
//    [self drawInRect:CGRectMake(0, 0, targetSize.width, targetSize.height)];
//    NSImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return scaledImage;
    NSRect targetFrame = NSMakeRect(0, 0, targetSize.width, targetSize.height);
    NSImage* targetImage = nil;
    NSImageRep *sourceImageRep =
    [self bestRepresentationForRect:targetFrame
                                   context:nil
                                     hints:nil];
    
    targetImage = [[NSImage alloc] initWithSize:targetSize];
    
    [targetImage lockFocus];
    [sourceImageRep drawInRect: targetFrame];
    [targetImage unlockFocus];
    
    return targetImage;
}

- (NSImage *)ra_imageByScalingToFitSize:(CGSize)targetSize
{
    CGFloat aspect = self.size.width / self.size.height;
    CGSize sizeToScale;
    if (targetSize.width / aspect <= targetSize.height) {
        sizeToScale = CGSizeMake(targetSize.width, targetSize.width / aspect);
    } else {
        sizeToScale = CGSizeMake(targetSize.height * aspect, targetSize.height);
    }
    
    return [self ra_imageByScalingToSize:sizeToScale];
}

@end
