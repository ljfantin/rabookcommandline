//
//  main.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 2/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAPicturesLoader.h"
#import <AppKit/AppKit.h>
#import "RAPostService.h"
#import "RAProccessPicturesTask.h"
#import <AFNetworking/AFNetworking.h>
#import "RaBookCommandLineApp.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSMutableArray *params = [NSMutableArray array];
        for (int i = 1; i < argc; i++) {
            NSString *str = [[NSString alloc] initWithCString:argv[i] encoding:NSUTF8StringEncoding];
            [params addObject:str];
        }
        
        RaBookCommandLineApp * app = [[RaBookCommandLineApp alloc] init];
        [app executeWithParams:params];
        NSLog(@"Exit");
    }
    return 0;
}
