//
//  RaBookUploadCommand.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 24/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RaBookCommand.h"

@interface RaBookUploadCommand : NSObject <RaBookCommand>

@end
