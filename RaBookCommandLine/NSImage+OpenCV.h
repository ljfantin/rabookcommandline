//
//  NSImage+OpenCV.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 5/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//
#include "opencv2/core.hpp"
#import <Cocoa/Cocoa.h>

@interface NSImage (OpenCV)

-(CGImageRef)CGImage;
-(cv::Mat)CVMat;

@end
