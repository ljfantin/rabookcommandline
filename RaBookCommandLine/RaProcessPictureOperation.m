//
//  RaProcessPictureOperation.m
//  RaBook
//
//  Created by Leandro Fantin on 28/11/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

// C++ headers
#include "RaRobustMatcher.hpp"

#import "RaProcessPictureOperation.h"
#import "RAFileUtils.h"
#import "NSImage+OpenCV.h"
#import "UIImageUtils.h"

@interface RaProcessPictureOperation ()

@property (nonatomic, assign) BOOL operationFinished;
@property (nonatomic, assign) BOOL operationExecuting;

@property (nonatomic, strong) NSImage *image;
@property (nonatomic, copy) RaProcessPictureOperationCompletionBlock completion;
@property (nonatomic, copy) RaProcessPictureOperationCancelBlock cancelBlock;
@property (nonatomic, copy) RaProcessPictureOperationErrorBlock errorBlock;

@end

@implementation RaProcessPictureOperation

- (instancetype)initWithImage:(NSImage *)image completionBlock:(RaProcessPictureOperationCompletionBlock)completionBlock cancelBlock:(RaProcessPictureOperationCancelBlock)cancelBlock errorBlock:(RaProcessPictureOperationErrorBlock)errorBlock;
{
    self = [super init];
    if (self) {
        self.image = image;
        self.completion = completionBlock;
        self.cancelBlock = cancelBlock;
        self.errorBlock = errorBlock;
    }
    return self;
}

- (void)finishOperation
{
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    
    self.operationExecuting = NO;
    self.operationFinished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

#pragma mark - NSOperation

- (BOOL)isExecuting
{
    return self.operationExecuting;
}

- (BOOL)isFinished
{
    return self.operationFinished;
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (void)start
{
    if ([self isCancelled]) {
        return;
    }
    RaRobustMatcher *robustMatcher = RaRobustMatcher::getRobustMatcherSharedInstance();
   // cv::Mat imageMat = [UIImageUtils cvMatFromUIImage:self.image];

    cv::Mat imageMat = [self.image CVMat];
    NSString *fileName = [[NSProcessInfo processInfo] globallyUniqueString];
    NSString *file = [RAFileUtils tempPathFileForObjectWithIdentifier:fileName fileType:@"json"];
    std::string path = std::string([file UTF8String]);
    robustMatcher->processImageAndSave(imageMat, path);
    [self finishOperation];
    if (self.completion) {
        self.completion(file);
    }
}

- (void)cancel
{
    if (![self isCancelled]) {
        [super cancel];
        if ([self isExecuting]) {
            [self finishOperation];
            if (self.cancelBlock) {
                self.cancelBlock();
            }
        }
    }
}


@end
