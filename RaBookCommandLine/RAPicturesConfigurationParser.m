//
//  RAPicturesConfigurationParser.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 3/7/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RAPicturesConfigurationParser.h"

@implementation RAPicturesConfigurationParser

+ (NSArray<RAPictureConfig *> *)loadConfigurationFromPath:(NSString *)path
{
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (!exists) {
        return nil;
    }
    NSArray *rootLevel = [[NSArray alloc] initWithContentsOfFile:path];
    
    NSMutableArray<RAPictureConfig *> *result = [NSMutableArray array];
    for (NSDictionary *dictionary in rootLevel) {
        RAPictureConfig *config = [[RAPictureConfig alloc] initWithDictionary:dictionary];
        [result addObject:config];
    }
    
    return result;
}

@end
