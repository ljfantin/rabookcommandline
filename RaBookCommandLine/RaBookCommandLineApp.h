//
//  RaBookCommandLineApp.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 18/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RaBookCommandLineApp : NSObject

- (void)executeWithParams:(NSArray<NSString *> *)params;

@end
