//
//  NSImage+Transformations.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 9/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

// TODO Hay que juntarlo con la categoria sobre iOS
@interface NSImage (Transformations)

- (NSImage *)ra_imageByScalingToSize:(CGSize)targetSize;
- (NSImage *)ra_imageByScalingToFitSize:(CGSize)targetSize;

@end
