//
//  RaBookCommandLineApp.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 18/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RaBookCommandLineApp.h"
#import "RAPicturesLoader.h"
#import "RAProccessPicturesTask.h"
#import "RaBookCommandLineMenu.h"
#import "RaBookCommand.h"
#import "RaBookUploadCommand.h"

@interface RaBookCommandLineApp ()

@property (nonatomic, strong) NSDictionary<NSString *,id<RaBookCommand>> *commands;

@end

@implementation RaBookCommandLineApp

- (instancetype)init
{
    self = [super init];
    if (self) {
        RaBookUploadCommand *uploadCommand = [[RaBookUploadCommand alloc] init];
        self.commands = @{@"upload" : uploadCommand};
    }
    return self;
}

- (BOOL)validParams:(NSArray<NSString *> *)params
{
    return params.count > 0 && self.commands[[params firstObject]] != nil;
}

- (void)executeWithParams:(NSArray<NSString *> *)params
{
    if ([self validParams:params]) {
        NSString *commandId = [params firstObject];
        id<RaBookCommand> command = self.commands[commandId];
        BOOL areValidParams = [command setParams:params];
        if (areValidParams) {
            [command execute];
        }
        else {
            // quizas aca podria mostrar un help para el command posta
            [RaBookCommandLineMenu showHelp];
        }
    } else {
        [RaBookCommandLineMenu showHelp];
    }
}

//- (void)uploadPicturesFromPath:(NSString *)path forClient:(NSString *)client deviceScreen:(CGSize)size
//{
//    NSArray <NSImage *> * pictures = [RAPicturesLoader loadPicturesFromPath:path];
//    RAProccessPicturesTask *task = [[RAProccessPicturesTask alloc] init];
//    [task uploadPicturesForClient:client deviceScreen:size pictures:pictures successBlock:^{
//        NSLog(@"Finish");
//    } cancelBlock:^{
//        NSLog(@"Cancel uploading");
//    } progressBlock:^(NSUInteger picturesUploaded) {
//        NSLog(@"Upload picture");
//    }];
//}

//- (void)uploadPicturesFromPath:(NSString *)path forClient:(NSString *)client
//{
//    NSArray <NSImage *> * pictures = [RAPicturesLoader loadPicturesFromPath:path];
//    RAProccessPicturesTask *task = [[RAProccessPicturesTask alloc] init];
//    for (NSValue *value in self.devicesScreen) {
//        CGSize size = [value sizeValue];
//        [task uploadPicturesForClient:client deviceScreen:size pictures:pictures successBlock:^{
//            NSLog(@"Finish");
//        } cancelBlock:^{
//            NSLog(@"Cancel uploading");
//        } progressBlock:^(NSUInteger picturesUploaded) {
//            NSLog(@"Upload picture");
//        }];
//    }
//}

@end
