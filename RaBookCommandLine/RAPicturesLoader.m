//
//  RAPicturesLoader.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 18/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RAPicturesLoader.h"
#import <AppKit/AppKit.h>
#import "NSFileManager+RaBook.h"

@implementation RAPicturesLoader

+ (NSArray <NSImage *> *)loadPicturesFromPath:(NSString *)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    //@"/Users/lfantin/personal"
    NSArray<NSURL *> * urls = [fm urlFromImagesAtPath:path];
    NSMutableArray<NSImage *> * pictures = [[NSMutableArray alloc] initWithCapacity:urls.count];
    for (NSURL *url in urls) {
        NSLog(@"URL %@", [url absoluteString]);
        NSImage * pic = [[NSImage alloc] initWithContentsOfURL:url];
        [pictures addObject:pic];
    }
    return pictures;
}

@end
