//
//  RAFileUtils.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 5/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RAFileUtils : NSObject

+ (NSString *)tempPathFileForObjectWithIdentifier:(NSString *)identifier fileType:(NSString *)type;

@end
