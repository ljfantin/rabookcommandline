//
//  RAProccessPicturesTask.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 5/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#include <stdio.h>
#include <RaBookEngine/RaFileStorage.hpp>

#import "RAProccessPicturesTask.h"
#import "RaProcessPictureOperation.h"
#import "RAArgumentedRealityObjectModel.h"
#import "RaArgumentedRealityObjectUploadOperation.h"
#import "NSImage+Transformations.h"

typedef void (^RaProcessPictureTaskAddPictureCompletionBlock)(void);
typedef void (^RaProcessPictureTaskAddPictureCancelBlock)(void);
typedef void (^RaProcessPictureTaskAddPictureErrorBlock)(NSError *error);


@interface RAProccessPicturesTask ()

@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSOperationQueue *serviceQueue;

@end

@implementation RAProccessPicturesTask

- (void)dealloc
{
    [self.operationQueue cancelAllOperations];
    [self.serviceQueue cancelAllOperations];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.operationQueue = [[NSOperationQueue alloc] init];
        self.operationQueue.maxConcurrentOperationCount = 1;
        
        self.serviceQueue = [[NSOperationQueue alloc] init];
        self.serviceQueue.maxConcurrentOperationCount = 3;
    }
    return self;
}

- (void)uploadPicturesForClient:(NSString *)client deviceScreen:(CGSize)deviceScreen pictures:(NSArray<RAPictureConfig *> *)pictures successBlock:(void (^)(void))successBlock cancelBlock:(void (^)(void))cancelBlock progressBlock:(void (^)(NSUInteger picturesUploaded))progressBlock
{
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];

    __block NSUInteger countPictureForUpload = pictures.count;
    for (RAPictureConfig * pictureConfig in pictures) {
        NSImage *picture = [[NSImage alloc] initWithContentsOfURL:pictureConfig.picturePath];
        NSLog(@"Uploading picture[Path: %@, thumbnailUrl: %@, layerIdentifier: %@]",pictureConfig.picturePath, pictureConfig.thumbnailUrl, pictureConfig.layerIdentifier);
        
        [self addPictureForClient:client picture:picture thumbnailURL:pictureConfig.thumbnailUrl layerType:pictureConfig.layerType layerIdentifier:pictureConfig.layerIdentifier deviceScreen:deviceScreen completionBlock:^{
            
            countPictureForUpload--;
            progressBlock(pictures.count - countPictureForUpload);

        } cancelBlock:^{
            
            countPictureForUpload--;
            progressBlock(pictures.count - countPictureForUpload);
           
        } errorBlock:^(NSError *error) {
           
            countPictureForUpload--;
            progressBlock(pictures.count - countPictureForUpload);
        }];
    }

    while(countPictureForUpload > 0 && [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:2]]);
    NSLog(@"Die");
    successBlock();
}

- (void)addPictureForClient:(NSString *)client picture:(NSImage *)picture thumbnailURL:(NSURL *)thumbnailURL layerType:(RAArgumentedRealityObjectLayerType)layerType layerIdentifier:(NSString *)layerIdentifier deviceScreen:(CGSize)size completionBlock:(RaProcessPictureTaskAddPictureCompletionBlock)completionBlock cancelBlock:(RaProcessPictureTaskAddPictureCancelBlock)cancelBlock errorBlock:(RaProcessPictureTaskAddPictureErrorBlock)errorBlock
{
    __weak typeof(self) weakSelf = self;
    CGSize areaDetector = [self areaDetectorFromSize:size];
    NSImage *pictureScale = [picture ra_imageByScalingToFitSize:areaDetector];
    NSImageRep *imgRep = [[pictureScale representations] firstObject];

    int imageWidth = (int)[imgRep pixelsWide];
    int imageHeight = (int)[imgRep pixelsHigh];
    
    // procesa la imagen
    RaProcessPictureOperation *processOperation = [[RaProcessPictureOperation alloc] initWithImage:pictureScale completionBlock:^(NSString *filePath) {
        
        //genera dto
        RAArgumentedRealityObjectModel *model = [weakSelf pictureModelFromFile:filePath width:imageWidth height:imageHeight layerType:layerType layerIdentifier:layerIdentifier client:client deviceScreen:size pictureUrl:thumbnailURL];
        
        // sube info a la nube
        RaArgumentedRealityObjectUploadOperation *uploadOperation = [[RaArgumentedRealityObjectUploadOperation alloc] initWithScreenSize:size objectModel:model completionBlock:^{
            
            if (completionBlock) {
                completionBlock();
            }
            
        } cancelBlock:^{
            if (cancelBlock) {
                cancelBlock();
            }
        } errorBlock:^(NSError *error) {
            if (errorBlock) {
                errorBlock(error);
            }
        }];
        
        [weakSelf.serviceQueue addOperation:uploadOperation];
    } cancelBlock:^{
        if (cancelBlock) {
            cancelBlock();
        }
        
    } errorBlock:^(NSError *error) {
        if (errorBlock) {
            errorBlock(error);
        }
    }];
    [self.operationQueue addOperation:processOperation];
}

// hay que migrar a model, ver porque seria una mescha de c++ y objc
- (RAArgumentedRealityObjectModel *)pictureModelFromFile:(NSString *)filePath width:(CGFloat)width height:(CGFloat)height layerType:(RAArgumentedRealityObjectLayerType)layerType layerIdentifier:(NSString *)layerIndentifier client:(NSString *)client deviceScreen:(CGSize)deviceScreen pictureUrl:(NSURL *)pictureUrl

{
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;
    RaFileStorage fs;
    fs.readFromFileSystem(std::string([filePath UTF8String]) , keypoints, descriptors);
    
    // pt x=345, y=40, size=31, angle=227.666, response=0.00001857, octave=0,class_id=-1
    NSMutableArray<NSNumber *> *arrayWithKeyPoints = [NSMutableArray array];
    for (std::vector<cv::KeyPoint>::iterator iterator = keypoints.begin(); iterator != keypoints.end(); ++iterator) {
        
        [arrayWithKeyPoints addObject:@(iterator->pt.x)];
        [arrayWithKeyPoints addObject:@(iterator->pt.y)];
        [arrayWithKeyPoints addObject:@(iterator->size)];
        [arrayWithKeyPoints addObject:@(iterator->angle)];
        [arrayWithKeyPoints addObject:@(iterator->response)];
        [arrayWithKeyPoints addObject:@(iterator->octave)];
        [arrayWithKeyPoints addObject:@(iterator->class_id)];
    }
    
    int size = descriptors.rows*descriptors.cols;
    NSMutableArray<NSNumber *> *arrayWithDescriptors = [NSMutableArray array];
    [arrayWithDescriptors addObject:@(descriptors.rows)];
    [arrayWithDescriptors addObject:@(descriptors.cols)];
    [arrayWithDescriptors addObject:@(descriptors.type())];
    if (descriptors.isContinuous()) {
        for (int index = 0; index < size; index++) {
            [arrayWithDescriptors addObject:@(descriptors.data[index])];
        }
    }

    return [[RAArgumentedRealityObjectModel alloc] initWithIdentifier:nil client:client size:deviceScreen pictureUrl:pictureUrl layerType:layerType layerIdentifier:layerIndentifier pictureWidth:width pictureHeight:height keypoints:arrayWithKeyPoints descriptors:arrayWithDescriptors];
    
}

// migrar a model
- (CGSize)areaDetectorFromSize:(CGSize)size
{
    // LEA AHORA estoy usando scale
    return CGSizeMake(size.width * 0.7f, size.height * 0.7f);
}

- (CGSize)thumbnailsSizeFromSize:(CGSize)size
{
    return CGSizeMake(size.width * 0.3f, size.height * 0.3f);
}



@end
