//
//  RaBookCommand.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 24/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RaBookCommand <NSObject>

- (BOOL)setParams:(NSArray *)params;
- (void)execute;

@end
