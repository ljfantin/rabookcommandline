//
//  NSFileManager+RaBook.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 11/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (RaBook)

- (NSArray<NSURL *> *)urlFromImagesAtPath:(NSString *)path;

@end
