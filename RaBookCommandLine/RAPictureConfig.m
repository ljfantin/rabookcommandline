//
//  RAPictureConfig.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 3/7/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RAPictureConfig.h"

static NSString *const kPicturePathKey = @"picture_path";
static NSString *const kThumbnailUrlKey = @"thumbnail_url";
static NSString *const kLayerIdentifierKey = @"layer_identifier";
static NSString *const kLayerTypeKey = @"layer_type";

static NSString *const kLayerTypeVimeoKey = @"vimeo";
static NSString *const kLayerTypeObjectKey = @"object";
static NSString *const kLayerTypeURLKey = @"url";

@interface RAPictureConfig ()

@end

@implementation RAPictureConfig

- (instancetype)initWithDictionary:(NSDictionary *)config
{
    self = [super init];
    if (self) {
        _picturePath = [NSURL URLWithString:config[kPicturePathKey]];
        _thumbnailUrl = [NSURL URLWithString:config[kThumbnailUrlKey]];
        _layerIdentifier = config[kLayerIdentifierKey];
        if ([config[kLayerTypeKey] isEqualToString:kLayerTypeVimeoKey]) {
            _layerType = RAArgumentedRealityObjectLayerTypeVimeo;
        }
        else
        if ([config[kLayerTypeKey] isEqualToString:kLayerTypeObjectKey]) {
            _layerType = RAArgumentedRealityObjectLayerTypeObject;
        }
        else {
            _layerType = RAArgumentedRealityObjectLayerTypeURL;
        }
    }
    return self;
}

@end
