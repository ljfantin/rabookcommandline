//
//  UIImageUtils.h
//  RaBook
//
//  Created by Leandro Fantin on 7/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "opencv2/core.hpp"
#import <Foundation/Foundation.h>

@interface UIImageUtils : NSObject

+ (cv::Mat)cvMatFromUIImage:(NSImage *)image;

@end
