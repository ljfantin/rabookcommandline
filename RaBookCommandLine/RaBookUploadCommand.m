//
//  RaBookUploadCommand.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 24/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RaBookUploadCommand.h"
#import "RAPicturesLoader.h"
#import "RAProccessPicturesTask.h"
#import "RAPicturesConfigurationParser.h"

@interface RaBookUploadCommand ()

@property (nonatomic, copy) NSString *pathConfiguration;
@property (nonatomic, copy) NSString *client;
@property (nonatomic, strong) NSArray *deviceScreens;
@end

@implementation RaBookUploadCommand

- (BOOL)setParams:(NSArray *)params
{
    NSMutableArray *screens = [[NSMutableArray alloc] init];
    if (params.count == 7 && [params[0] isEqualToString:@"upload"] && [params[1] isEqualToString:@"--pathConfiguration"] && [params[3] isEqualToString:@"--client"] && [params[5] isEqualToString:@"--deviceScreen"]) {

        self.pathConfiguration = params[2];
        self.client = params[4];
        NSString *size = params[6];
        if ([size isEqualToString:@"ALL"]) {
            
            // portrait
            [screens addObject:[NSValue valueWithSize:CGSizeMake(320, 480)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(640, 960)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(640, 1136)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(750, 1334)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(1125, 2001)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(1242, 2208)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(1536, 2048)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(2048, 2732)]];
            
            // landscape
            [screens addObject:[NSValue valueWithSize:CGSizeMake(480, 320)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(960, 640)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(1136, 640)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(1334, 750)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(2001, 1125)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(2208, 1242)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(2048, 1536)]];
            [screens addObject:[NSValue valueWithSize:CGSizeMake(2732, 2048)]];
            
        } else {
            
            NSArray *arrayWithSizes = [size componentsSeparatedByString:@"x"];
            NSString *width = [arrayWithSizes firstObject];
            NSString *height = [arrayWithSizes lastObject];
            [screens addObject: [NSValue valueWithSize:CGSizeMake(width.floatValue, height.floatValue)]];
        }
        self.deviceScreens = screens;
        return YES;
    }
    return NO;
}

- (void)execute
{
    NSLog(@"Process pictures");
    for (NSValue *screen in self.deviceScreens) {
        CGSize deviceScreen = [screen sizeValue];
        NSLog(@"\nScreen Size: %@", NSStringFromSize(deviceScreen));
        NSArray<RAPictureConfig *> * pictures = [RAPicturesConfigurationParser loadConfigurationFromPath:self.pathConfiguration];
        RAProccessPicturesTask *task = [[RAProccessPicturesTask alloc] init];
        [task uploadPicturesForClient:self.client deviceScreen:deviceScreen pictures:pictures successBlock:^{
            NSLog(@"Finish");
        } cancelBlock:^{
            NSLog(@"Cancel uploading");
        } progressBlock:^(NSUInteger picturesUploaded) {
            NSLog(@"Upload picture");
        }];
    }
}

@end
