//
//  RAFileUtils.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 5/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "RAFileUtils.h"

@implementation RAFileUtils

+ (NSString *)tempPathFileForObjectWithIdentifier:(NSString *)identifier fileType:(NSString *)type
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [paths firstObject];
    // un identifier puede tener un nombre asi 9EAA60BC-201C-4D06-9FD4-5A95F1A15051/L0/001
    // por las barras no lo puede poner de nombre, porque entiende que es directorio
    NSString * identifierWithoutSlashes = [identifier stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    // identifierWithoutSlashes = [identifierWithoutSlashes stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString * pathWithFile = [path stringByAppendingPathComponent:identifierWithoutSlashes];
    // NSString *fileName = [NSString  stringWithFormat:@"obj%@", @"vol"];
    //pathWithFile = [path stringByAppendingPathComponent:pathWithFile];
    return [NSString  stringWithFormat:@"%@.%@", pathWithFile, type];
    
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString *docs = [paths objectAtIndex:0];
     return [docs stringByAppendingPathComponent:@"vocabulary.xml"];*/
    
}

@end
