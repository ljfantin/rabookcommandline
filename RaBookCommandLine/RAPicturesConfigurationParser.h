//
//  RAPicturesConfigurationParser.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 3/7/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAPictureConfig.h"

@interface RAPicturesConfigurationParser : NSObject

+ (NSArray<RAPictureConfig *> *)loadConfigurationFromPath:(NSString *)path;

@end
