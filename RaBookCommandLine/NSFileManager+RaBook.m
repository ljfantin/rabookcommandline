//
//  NSFileManager+RaBook.m
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 11/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import "NSFileManager+RaBook.h"

@implementation NSFileManager (RaBook)

- (NSArray<NSURL *> *)urlFromImagesAtPath:(NSString *)path
{
    NSString *pathWithImages = path?path:[self currentDirectoryPath];
    NSURL *urlForPathWithImages = [NSURL fileURLWithPath:pathWithImages];
    NSMutableArray<NSURL *> * result = [[NSMutableArray alloc] init];
    NSArray<NSString *> * arrayWithContents = [self contentsOfDirectoryAtPath:pathWithImages error:nil];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"];
    NSArray<NSString *> *onlyJPGs = [arrayWithContents filteredArrayUsingPredicate:fltr];
    for (NSString *stringWithUrl in onlyJPGs) {
        [result addObject:[NSURL URLWithString:stringWithUrl relativeToURL:urlForPathWithImages]];
    }
    return result;
}

@end
