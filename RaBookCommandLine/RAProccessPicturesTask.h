//
//  RAProccessPicturesTask.h
//  RaBookCommandLine
//
//  Created by Leandro Fantin on 5/5/17.
//  Copyright © 2017 Leandro Fantin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RAPictureConfig.h"

@interface RAProccessPicturesTask : NSObject

- (void)uploadPicturesForClient:(NSString *)client deviceScreen:(CGSize)deviceScreen pictures:(NSArray<RAPictureConfig *> *)objects successBlock:(void (^)(void))successBlock cancelBlock:(void (^)(void))cancelBlock progressBlock:(void (^)(NSUInteger picturesUploaded))progressBlock;

@end
